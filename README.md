# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
1. 最上方空格:當前功能
2. XXsize:該功能的畫筆大小
3. color:畫筆顏色
4. Line:滑鼠按住畫線，放開完成線條 
5. Eraser:清空特定區域
6. T:文字輸入，在畫布上任一處點選，出現文字方塊。打字後雙擊滑鼠完成。
7. Circle:按住拖曳圓形，放開完成
8. Rectangle:按住拖曳方形，放開完成
9. Triangle:按住拖曳三角形，放開完成
10. fill:內部是否填滿
11. Redo:重做
12 .Undo:回復
13. Reset:清空畫布(不清空Redo/Undo步驟)
14. Download:當前畫布的畫面下載
15. Upload:先點右邊"選擇檔案"按鈕，點選Upload，拖曳想要的大小範圍，放開完成。
16. Image Preview:上傳圖片預覽
17. fixed:圖片是否要依原比例繪製
18. stored steps:已儲存步驟



