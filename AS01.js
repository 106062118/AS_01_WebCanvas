var x;
var y;
var flag;
var tflag;
var memx;
var memy;
var lastx;
var lasty;
var lastchange;
var Record = new Array();
var ToolRec;
var step = new Array();
var Layer = 0;

window.onload = function(){
    change('Line');
    Download();
    draw();
    RecImg();
}

function change(e){
    var c = document.getElementById("myCanvas");
    var c2 = document.getElementById("Canvas2");
    if( e == 'Line'){
        c.style.cursor = "cell";
        c2.style.cursor = "cell";
        document.getElementById("sizename").innerText = "Painter size";
    }
    if( e == 'Eraser'){
        c.style.cursor = "url(erasericon.png) 6 58, auto";
        c2.style.cursor = "url(erasericon.png) 6 58, auto";
        document.getElementById("sizename").innerText = "Eraser size";
    }
    if(e == 'T' && lastchange != 'T'){
        var tpf = document.createElement("INPUT");
        c.style.cursor = "text";
        c2.style.cursor = "text";
        document.getElementById("typefacename").innerText = "Type face";
        document.getElementById("sizename").innerText = "Font size";
        document.getElementById("table1").appendChild(tpf);
        tpf.id = "typeface";
        tpf.value = "Arial";
        tflag = 1;
    }
    if(e == 'Reset'){
        e = document.getElementById("screen").value;
        c.getContext("2d").clearRect(0, 0, 800, 600);
        RecImg();
    }
    if(e == 'Upload'){
        var imgshow = document.getElementById("imgshow");
        var img = new Image();
        var url = URL.createObjectURL(document.getElementById("File").files[0]);
        c.style.cursor = "cell";
        c2.style.cursor = "cell";
        img.src = url;
        img.onload = function(){
            imgshow.src = img.src;
            if(img.height*300/img.width > 300){
                imgshow.width = img.width*300/img.height;
                imgshow.height = 300;
            }
            else {
                imgshow.width = 300;
                imgshow.height = img.height*300/img.width;
            }
        }
    }
    if(e == 'Circle' || e == 'Rectangle' || e == 'Triangle' ){
        c.style.cursor = "cell";
        c2.style.cursor = "cell";
    }

    if(e != 'T' && lastchange == 'T'){
        document.getElementById("typefacename").innerText = "";
        document.getElementById("typeface").remove();
    }
    lastchange = e;
    document.getElementById("screen").value = e;
}

function draw(){
    var tex = document.getElementById("screen");
    Record[0] = new Array();
    step[0] = -1;
    document.getElementById("canv").addEventListener("mousedown",function(){
        var c = document.getElementById("myCanvas");
        var ctx = c.getContext("2d");
        if(tex.value == "Line"){
            ctx.beginPath();
            ctx.moveTo(x,y);
        }
        if(tex.value == 'Circle' || tex.value == 'Rectangle' || tex.value == 'Triangle' || tex.value == 'Upload'){
            memx = x;
            memy = y;
            flag = 1;
            ToolRecImg();
        }
        mode();
        document.getElementById("canv").addEventListener("mousemove", mode);
    });

    document.getElementById("canv").addEventListener("mouseup",function(){
        if(flag == 1){
            if(tex.value == "Circle")circle(0);
            if(tex.value == "Rectangle")rectangle(0);
            if(tex.value == "Triangle")triangle(0);
            if(tex.value == "Upload")upload(0);
        }
        document.getElementById("canv").removeEventListener("mousemove", mode);
        ToolUndo();
        flag = 0;
        RecImg();
    });

    document.getElementById("canv").addEventListener("mouseout",function(){
        document.getElementById("canv").removeEventListener("mousemove", mode);
        if(flag == 1)ToolUndo();
        flag = 0;
        RecImg();
    });

    document.getElementById("canv").addEventListener("dblclick",function(){
        if(tflag == 0){
            var ctx = document.getElementById("myCanvas").getContext("2d");
            ctx.font = document.getElementById("paintersize").value+"px"+" "+document.getElementById("typeface").value;
            ctx.fillStyle = document.getElementById("colorpick").value;
            ctx.fillText(document.getElementById("txt").value, memx, memy);
            document.getElementById("txt").remove();
            tflag = 1;
            RecImg();
        }
    });
}

function mode(){
    var tex = document.getElementById("screen");
    if(tex.value == 'Line')line();
    if(tex.value == 'Eraser')eraser();

    if(tex.value == 'Circle'){
        ToolUndo();
        circle(1);
    }
    if(tex.value == 'Rectangle'){
        ToolUndo();
        rectangle(1);
    }
    if(tex.value == 'Triangle'){
        ToolUndo();
        triangle(1);
    }
    if(tex.value == 'Upload'){
        ToolUndo();
        upload(1);
    }
    if(tex.value == 'T')texttype();
}

function path(event){
    x = event.offsetX;
    y = event.offsetY;
}

function line(){
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    ctx.lineJoin="round";
    ctx.strokeStyle = document.getElementById("colorpick").value;
    ctx.lineWidth = document.getElementById("paintersize").value;
    ctx.lineTo(x,y);
    ctx.stroke();
}

function eraser(){
    var c = document.getElementById("myCanvas")
    var ctx = c.getContext("2d");
    var size = document.getElementById("paintersize").value;
    ctx.clearRect(x-size/2, y-size/2, size, size);
}

function circle(s){
    var c;
    if( s == 0 )c = document.getElementById("myCanvas");
    else c = document.getElementById("Canvas2");
    var ctx = c.getContext("2d");
    ctx.beginPath();
    ctx.fillStyle = document.getElementById("colorpick").value;
    ctx.arc(memx, memy, Math.sqrt(Math.pow(x-memx, 2)+Math.pow(y-memy, 2)), 0, 2*Math.PI);
    if(document.getElementById("Crcchk").checked == true)ctx.fill();
    else {
        ctx.lineJoin="miter";
        ctx.lineWidth = document.getElementById("paintersize").value;
        ctx.stroke();
    }
}

function rectangle(s){
    var c;
    if( s == 0 )c = document.getElementById("myCanvas");
    else c = document.getElementById("Canvas2");
    var ctx = c.getContext("2d");
    ctx.beginPath();
    ctx.fillStyle = document.getElementById("colorpick").value;
    ctx.rect(memx, memy, x-memx, y-memy);
    if(document.getElementById("Recchk").checked == true)ctx.fill();
    else {
        ctx.lineJoin="miter";
        ctx.lineWidth = document.getElementById("paintersize").value;
        ctx.stroke();
    }
}

function triangle(s){
    var c;
    if( s == 0 )c = document.getElementById("myCanvas");
    else c = document.getElementById("Canvas2");
    var ctx = c.getContext("2d");
    ctx.fillStyle = document.getElementById("colorpick").value;
    ctx.beginPath();
    ctx.moveTo(memx,y);
    ctx.lineTo(x, y);
    ctx.lineTo((memx+x)/2,memy);
    ctx.closePath();
    if(document.getElementById("Trichk").checked == true)ctx.fill();
    else {
        ctx.lineJoin="miter";
        ctx.lineWidth = document.getElementById("paintersize").value;
        ctx.stroke();
    }
}

function texttype(){
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    if(tflag != 0){
        memx = x;
        memy = y;
        var txt = document.createElement("INPUT");
        document.getElementById("canv").appendChild(txt);
        txt.style.position = "absolute";
        txt.style.top = y-document.getElementById("paintersize").value+"px";
        txt.style.left = x+"px";
        txt.id = "txt";
        txt.style.fontSize = document.getElementById("paintersize").value+"px"; 
        txt.style.fontFamily = document.getElementById("typeface").value;
        tflag = 0;
    }
}

function upload(s){
    var c;
    var img = new Image();
    var chk = document.getElementById("ratiochk").checked;
    var newx;
    var newy;
    img.src = document.getElementById("imgshow").src;
    newx = (x>memx)?1:-1;
    newy = (y>memy)?1:-1;
    if( s == 0 ){
        c = document.getElementById("myCanvas");
        var ctx = c.getContext("2d");
        if(chk == true){
            ctx.drawImage(img, memx, memy, 
                Math.sqrt(Math.pow(x-memx,2)+Math.pow(y-memy,2))/Math.sqrt(Math.pow(img.width,2)+Math.pow(img.height,2))*img.width*newx, 
                Math.sqrt(Math.pow(x-memx,2)+Math.pow(y-memy,2))/Math.sqrt(Math.pow(img.width,2)+Math.pow(img.height,2))*img.height*newy);
        }
        else {
            img.onload = function() { 
                ctx.drawImage(img, memx, memy, x-memx, y-memy);
            }
        }
    }
    else {
        c = document.getElementById("Canvas2");
        var ctx = c.getContext("2d");
        ctx.beginPath();
        if(chk == true){
            ctx.rect(memx, memy, 
                Math.sqrt(Math.pow(x-memx,2)+Math.pow(y-memy,2))/Math.sqrt(Math.pow(img.width,2)+Math.pow(img.height,2))*img.width*newx, 
                Math.sqrt(Math.pow(x-memx,2)+Math.pow(y-memy,2))/Math.sqrt(Math.pow(img.width,2)+Math.pow(img.height,2))*img.height*newy);
            ctx.stroke();
        }
        else{
            ctx.rect(memx, memy, x-memx, y-memy);
            ctx.stroke();
        }
    }
}

function Download(){
    var c = document.getElementById("Download");
    var url = document.getElementById("myCanvas").toDataURL();
    var img = new Image();
    img.src = url;
    img.onload = function(){
        c.href = img.src;
    }
    
}

function layer(){
    
}

function RecImg(){
    if(document.getElementById("myCanvas").toDataURL() != Record[0][step]){
        step++;
        if (step < Record[0].length) { Record[0].length = step;}
        Record[0].push(document.getElementById("myCanvas").toDataURL());
        Download();
        document.getElementById("demo").innerText = "Stored steps:"+step+"/"+(Record[0].length-1);
    }
}

function Redo(){
    if(step < Record[0].length-1){
        var ctx = document.getElementById("myCanvas").getContext("2d");
        step++;
        var cpc = new Image();
        cpc.src = Record[0][step];
        ctx.clearRect(0, 0, 800, 600);
        cpc.onload = function () {
            ctx.drawImage(cpc, 0, 0); 
            Download();
            document.getElementById("demo").innerText = "Stored steps:"+step+"/"+(Record[0].length-1);
        }
    }
}

function Undo(){
    if(step > 0){
        var ctx = document.getElementById("myCanvas").getContext("2d");
        step--;
        var cpc = new Image();
        cpc.src = Record[0][step];
        ctx.clearRect(0, 0, 800, 600);
        cpc.onload = function () { 
            ctx.drawImage(cpc, 0, 0);
            Download();
            document.getElementById("demo").innerText = "Stored steps:"+step+"/"+(Record[0].length-1);
        }
    }
}

function ToolRecImg(){
    ToolRec = document.getElementById("myCanvas").toDataURL();
}

function ToolUndo(){
    var ctx = document.getElementById("Canvas2").getContext("2d");
    var cpc = new Image();
    cpc.src = ToolRec;
    ctx.clearRect(0, 0, 800, 600);
}